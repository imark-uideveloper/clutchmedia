(function($) {
  'use strict';
  
	jQuery(document).ready(function(){
    
		// Wow Js
		var wow = new WOW({
			mobile: false // default
		})
		wow.init();
		
		// Aos Js
		AOS.init({
			easing: 'ease-in-out-sine'
		});
		
		// Page Scroll
		jQuery('a[href*="#"]:not([href="#"])').click(function () {
			if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
				var target = jQuery(this.hash);
				target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
				if (target.length) {
					jQuery('html, body').animate({
						scrollTop: target.offset().top
					}, 1000);
					return false;
				}
			}
		});
	});

});
$(document).ready(function(){
	$('.news-carousel').owlCarousel({
	autoplay: 3000,
	loop: true,
	margin: 20, 
	responsiveClass: true,
	items:2,
	nav: true,
});
});

$(document).ready(function(){
	$('.services-carousel').owlCarousel({
	autoplay: 3000,
	//loop: true,
	margin: 20, 
	responsiveClass: true,
	items:1,
	nav: true,
});
});

///////////////////////////////////////////////////////////////////////////////////////////////
var wow = new WOW({
    boxClass: 'wow', // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset: 0, // distance to the element when triggering the animation (default is 0)
    mobile: true, // trigger animations on mobile devices (default is true)
    live: true, // act on asynchronously loaded content (default is true)
    callback: function (box) {
        // the callback is fired every time an animation is started
        // the argument that is passed in is the DOM node being animated
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
});
wow.init();
 