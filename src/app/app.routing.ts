import { Routes, RouterModule } from '@angular/router';


import { AppLayoutComponent } from './layout/app-layout/app-layout-component';
import { HomeComponent } from './layout/home/home.component';
import { AboutComponent } from './layout/about/about.component';
import { ContactComponent } from './layout/contact/contact.component';
import { ProjectComponent } from './layout/project/project.component';
import { PortfoliosingleComponent } from './layout/portfoliosingle/portfoliosingle.component';
import { BlogComponent } from './layout/blog/blog.component';
import { BloginternalComponent } from './layout/blog-internal/blog-internal.component';
import { ServicesComponent } from './layout/services/services.component';
import { ServicesinternalComponent } from './layout/services-internal/services-internal.component';


const appRoutes: Routes = [
    {
        path: '',
        component: AppLayoutComponent,
        children: [
            { path: '', component: HomeComponent, pathMatch: 'full' },
            { path: 'about', component: AboutComponent },
            { path: 'contact', component: ContactComponent },
            { path: 'project', component: ProjectComponent },
            { path: 'portfoliosingle', component: PortfoliosingleComponent },
            { path: 'blog', component: BlogComponent },
            { path: 'blog-internal', component: BloginternalComponent },
            { path: 'blog-internal', component: BloginternalComponent },
            { path: 'services', component: ServicesComponent },
            { path: 'services-internal', component: ServicesinternalComponent },
        ]
    },  
];

export const routing = RouterModule.forRoot(appRoutes);
