import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
//import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { AppLayoutComponent } from './layout/app-layout/app-layout-component';
import { AppHeaderComponent } from './layout/app-header/app-header-component';
import { HomeComponent } from './layout/home/home.component';
import { AboutComponent } from './layout/about/about.component';
import { ContactComponent } from './layout/contact/contact.component';
import { ProjectComponent } from './layout/project/project.component';
import { PortfoliosingleComponent } from './layout/portfoliosingle/portfoliosingle.component';
import { BlogComponent } from './layout/blog/blog.component';
import { BloginternalComponent } from './layout/blog-internal/blog-internal.component';
import { ServicesComponent } from './layout/services/services.component';
import { ServicesinternalComponent } from './layout/services-internal/services-internal.component';
import { AppFooterComponent } from './layout/app-footer/app-footer-component';
  

import { routing } from './app.routing';

@NgModule({
  imports:      [ BrowserModule, FormsModule, routing ],
  declarations: [ AppComponent, AppHeaderComponent, AppLayoutComponent, HomeComponent, AboutComponent, AppFooterComponent, ContactComponent, ProjectComponent, BlogComponent, ServicesComponent, BloginternalComponent, ServicesinternalComponent, PortfoliosingleComponent  ],
  bootstrap:    [ AppComponent ]
}) 
export class AppModule { 
  
} 